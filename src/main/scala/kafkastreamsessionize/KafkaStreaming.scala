package kafkastreamsessionize

import org.apache.kafka.common.serialization.{Serde, Serdes}
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.StreamsConfig._
import org.apache.kafka.streams.kstream._
import org.apache.kafka.streams.KeyValue
import java.util.Properties

import dataholders.UserEvent
import org.apache.kafka.clients.consumer.ConsumerConfig


object KafkaStreaming {

  def main(args: Array[String]): Unit = {
    val streamsConfiguration = new Properties()
    streamsConfiguration.put(APPLICATION_ID_CONFIG, "Streaming-Sessionization")
    streamsConfiguration.put(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
    streamsConfiguration.put(DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String.getClass.getName)
    streamsConfiguration.put(DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String.getClass.getName)
    streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")

    //define session window
    SessionWindows.`with`(60000).until(3000000)

    val builder = new KStreamBuilder
    val originalStream: KStream[String, String] = builder.stream("TQBQTEST_USER_TEST")

    val mappedUserEventStream: KStream[String, UserEvent] = originalStream.mapValues(new ValueMapper[String, UserEvent] {
      override def apply(jsonString: String): UserEvent = UserEvent(jsonString)
    })

    //making uid the key for each record
    val StreamWithUIDKey: KStream[String, UserEvent] = mappedUserEventStream
      .map(new KeyValueMapper[String, UserEvent, KeyValue[String, UserEvent]] {
        override def apply(key: String, value: UserEvent): KeyValue[String, UserEvent] =
          new KeyValue(value.uid, value)
    })

    StreamWithUIDKey.foreach(new ForeachAction[String, UserEvent] {
            override def apply(key: String, value: UserEvent): Unit = println(key)
          })

    val stream = new KafkaStreams(builder, streamsConfiguration)

    //local reset of the application
    stream.cleanUp()

    stream.start()
    println()
  }
}
