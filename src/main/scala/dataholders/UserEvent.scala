package dataholders

import net.liftweb.json.parse

case class UserEvent(timestamp: String, merchantId: String, cid: String, uid: String,
                     sessionId: String, event: String, eventType: String, ip: String,
                     refUrl: String, referrer: String)



object UserEvent {

  def apply(json: String): UserEvent = {
    implicit val formats = net.liftweb.json.DefaultFormats
    parse(json).extract[UserEvent]
  }
}